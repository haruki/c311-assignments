#lang racket

;; an interpreter that argument-passing is call-by-need.

;; author: YaoSen Wang

(define empty-env
  (lambda ()
    (lambda(y)
      (error 'value-of "unbound variable ~s" y))))


(define extend-env
  (lambda (y v old-env)
    (lambda (query)
      (if (eqv? y query)
          v
          (apply-env old-env query)))))


(define apply-env
  (lambda (env y)
    (env y)))


(define unbox/need
  (lambda (y)
    (let ([val ((unbox y))])
      (set-box! y (lambda () val))
      val)))


(define make-closure
  (lambda (x body env)
    (lambda (d d-env)
      (match d
        [`,y #:when (symbol? y)
             (val-of-cbneed body
                            (extend-env x (box (apply-env d-env d)) env))]
        [else
         (val-of-cbneed body
                        (extend-env x (box (lambda () (val-of-cbneed d d-env))) env))]))))


(define apply-closure
  (lambda (c d d-env)
    (c d d-env)))

(define val-of-cbneed
  (lambda (exp env)
    (match exp
      [`,b #:when (boolean? b) b]

      [`,n #:when (number? n)  n]

      [`(zero? ,n)
       (zero? (val-of-cbneed n env))]

      [`(sub1 ,n)
       (sub1 (val-of-cbneed n env))]

      [`(* ,n1 ,n2)
       (* (val-of-cbneed n1 env) (val-of-cbneed n2 env))]

      [`(if ,test ,conseq ,alt) (if (val-of-cbneed test env)
                                    (val-of-cbneed conseq env)
                                    (val-of-cbneed alt env))]

      [`(let ([,x ,e]) ,body)
       (let ([new-env (extend-env x (box (lambda () (val-of-cbneed e env))) env)])
         (val-of-cbneed body new-env))]

      [`(begin2 ,e1 ,e2)
       (begin (val-of-cbneed e1 env) (val-of-cbneed e2 env))]

      [`(set! ,y ,e)
       (let ([b (apply-env env y)]
             [v (lambda () (val-of-cbneed e env))])
         (set-box! b v))]

      [`(random ,n) (random (val-of-cbneed n env))]

      [`,y #:when (symbol? y)
           (unbox/need (apply-env env y))]

      [`(lambda (,x) ,body)
       (make-closure x body env)]

      [`(,rator ,rand)
       (apply-closure (val-of-cbneed rator env) rand env)])))


;; tests

(define random-sieve
  '((lambda (n)
      (if (zero? n)
          (if (zero? n)
              (if (zero? n)
                  (if (zero? n)
                      (if (zero? n)
                          (if (zero? n)
                              (if (zero? n)
                                  #t
                                  #f)
                              #f)
                          #f)
                      #f)
                  #f)
              #f)
          (if (zero? n) #f (if (zero? n) #f (if (zero? n) #f (if (zero? n) #f (if (zero? n) #f (if (zero? n) #f #t))))))))
    (random 2)))

(val-of-cbneed random-sieve (empty-env))
; #t


(val-of-cbneed
 '(let ([x ((lambda (x) (x x)) (lambda (x) (x x)))])
    ((lambda (x) 100) x))
 (empty-env))
; 100


